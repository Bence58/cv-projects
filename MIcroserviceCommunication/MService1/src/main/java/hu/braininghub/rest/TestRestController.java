package hu.braininghub.rest;

import hu.braininghub.obj.TestObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class TestRestController {

    @Autowired
    TestObj test;

    @Value("${server.port}")
    private int thisMicroservicePort;

    @RequestMapping(method = RequestMethod.GET, path = "test")
    public String test() {
        return "respond";
    }

    @RequestMapping(path = "obj")
    public ResponseEntity getTestObj() {
        test.setBrand("Awesome Brand");
        test.setName("TEST");
        test.setCurrentPortNum(thisMicroservicePort);
        test.setMessage("Ez az uzi a Microservice1-bol jon yeey!");
        return ResponseEntity.ok(test);
    }
}

