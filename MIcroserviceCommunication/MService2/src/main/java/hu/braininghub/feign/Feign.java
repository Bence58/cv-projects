package hu.braininghub.feign;

import hu.braininghub.obj.TestObj;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("MService1")
public interface Feign {

    @RequestMapping(path = "obj")
    TestObj testObj();

}
