package hu.braininghub.rest;

import hu.braininghub.feign.Feign;
import hu.braininghub.obj.TestObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/")
public class Rest {

    @Autowired
    private Feign f;

    @Autowired
    private TestObj testobj;

    @Value("${server.port}")
    private int thisMicroservicePort;


    @RequestMapping(method = RequestMethod.GET, path = "test")
    public String getResponse() {
        return "server Response";
    }

    @RequestMapping(path = "obj2", method = RequestMethod.GET)
    public ResponseEntity getTestObj() {
        testobj = f.testObj();
        testobj.setCurrentPortNum(thisMicroservicePort);
        return ResponseEntity.ok(testobj);
    }

}