package hu.braininghub.service;


import hu.braininghub.dto.CarDTO;
import hu.braininghub.entity.Car;
import hu.braininghub.repository.CarRepository;
import hu.braininghub.request.SearchBrand;
import hu.braininghub.request.SearchTwoParam;
import hu.braininghub.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    @Autowired
    CarRepository cr;

    public void saveNewCar(CarDTO cardto) {

        cr.saveNewCar(Mapper.mapCarDtToEntity(cardto));

    }

    public List<CarDTO> getCarsByBrand(SearchBrand sb) {

        return Mapper.mapCarToCarDTO(cr.getCarsByBrand(sb.getBrandName()));

    }

    public List<CarDTO> getCarsByBrandAndModel(SearchTwoParam stp) {

        return Mapper.mapCarToCarDTO(cr.getCarsByBrandAndModel(stp.getBrand(), stp.getModel()));

    }

    public List<CarDTO> getAllCar(){
        return Mapper.mapCarToCarDTO(cr.getAllCar());
    }


}
