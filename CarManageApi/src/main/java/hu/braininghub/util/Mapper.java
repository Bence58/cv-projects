package hu.braininghub.util;

import hu.braininghub.dto.CarDTO;
import hu.braininghub.entity.Car;
import org.springframework.beans.BeanUtils;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Mapper {


    public static Car mapCarDtToEntity(CarDTO cardto) {
        Car car = new Car();

        BeanUtils.copyProperties(cardto, car);
        car.setId(null);
        car.setRegtime(Date.valueOf(LocalDate.now()));


        return car;

    }

    public static List<CarDTO> mapCarToCarDTO(List<Car> carlist) {
        List<CarDTO> listOfCars = new ArrayList<>();

        carlist.forEach(p ->
                {
                    CarDTO crdto = new CarDTO();
                    BeanUtils.copyProperties(p,crdto);
                    listOfCars.add(crdto);


                });

        return listOfCars;

    }

}
