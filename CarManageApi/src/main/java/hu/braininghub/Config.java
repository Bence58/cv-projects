package hu.braininghub;

import hu.braininghub.entity.Car;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "hu.braininghub.repository")
@ComponentScan(basePackages = "hu.braininghub")
@EntityScan(basePackageClasses = Car.class)
@PropertySource("classpath:application.properties")
public class Config {
}
