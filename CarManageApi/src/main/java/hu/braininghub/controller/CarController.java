package hu.braininghub.controller;

import hu.braininghub.dto.CarDTO;
import hu.braininghub.request.SearchBrand;
import hu.braininghub.request.SearchTwoParam;
import hu.braininghub.response.SaveSucess;
import hu.braininghub.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "V1")
public class CarController {

    @Autowired
    SaveSucess ss;

    @Autowired
    CarService cs;

    @RequestMapping(path = "ServerStatus", method = RequestMethod.GET)
    public String serverRespond() {

        return "Server Respond";

    }

    @RequestMapping(path = "Save", method = RequestMethod.POST)
    public ResponseEntity saveCar(@RequestBody CarDTO cardto) {


        cs.saveNewCar(cardto);
        ss.setResponseText("We got your save request!");
        return ResponseEntity.ok(ss);
    }

    @RequestMapping(path = "SearchByBrand", method = RequestMethod.POST)
    public ResponseEntity searcCarsByBrandName(@RequestBody SearchBrand sb) {
        return ResponseEntity.ok(cs.getCarsByBrand(sb));

    }

    @RequestMapping(path = "SearchByBrandAndModel", method = RequestMethod.POST)
    public ResponseEntity searcCarsByModelAndBrand(@RequestBody SearchTwoParam stp){

        return ResponseEntity.ok(cs.getCarsByBrandAndModel(stp));

    }

    @RequestMapping(path = "GetAllCar", method = RequestMethod.GET)
    public ResponseEntity getAllCar(){

        return ResponseEntity.ok(cs.getAllCar());
    }

}
