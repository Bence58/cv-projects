package hu.braininghub.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SearchTwoParam {
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("model")
    private String model;
}
