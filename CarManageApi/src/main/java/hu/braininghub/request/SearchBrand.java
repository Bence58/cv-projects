package hu.braininghub.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.inject.Singleton;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Component
public class SearchBrand {

    @JsonProperty("brandname")
    private String brandName;

}
