package hu.braininghub.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class CarDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("platenumber")
    private String plateNumber;
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("color")
    private String color;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("model")
    private String model;
    @JsonProperty("regtime")
    private Date regtime;


}
