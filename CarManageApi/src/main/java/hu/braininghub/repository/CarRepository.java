package hu.braininghub.repository;


import hu.braininghub.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CarRepository {

    @Autowired
    EntityManager em;

    @Transactional
    public void saveNewCar(Car car) {

        em.persist(car);
    }

    @Transactional
    public List<Car> getCarsByBrand(String brandName) {
        Query q = em.createQuery("from Car where brand=:brandname");
        q.setParameter("brandname", brandName);
        return q.getResultList();

    }

    @Transactional
    public List<Car> getCarsByBrandAndModel(String brandName, String model) {
        Query q = em.createQuery("from Car where brand=:brandname and model=:model");
        q.setParameter("brandname", brandName);
        q.setParameter("model", model);
        return q.getResultList();

    }

    public List<Car> getAllCar(){

        Query q = em.createQuery("from Car");
        return q.getResultList();
    }


}
